import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { LayoutComponent } from './layout/layout.component';
import { ClarityModule } from '@clr/angular';
import { HomeComponent } from './home/home.component';
import { UsersComponent } from './users/users.component';
import { UsersService } from './users.service';

@NgModule({
  imports: [
    CommonModule,
    ClarityModule,
    AdminRoutingModule
  ],
  declarations: [LayoutComponent, HomeComponent, UsersComponent],
  providers: [UsersService]
})
export class AdminModule { }
