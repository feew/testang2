import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
    @Inject('API_URL') private apiUrl: string,
    private httpClient: HttpClient
  ) { }

  async doLogin(username: string, password: string) {
    const url = this.apiUrl + '/login';
    const data: any = {
      username: username,
      password: password
    }
    return await this.httpClient.post(url, data).toPromise();
  }
}

