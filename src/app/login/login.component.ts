import { Component, OnInit } from '@angular/core';
import { LoginService } from './login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: []
})
export class LoginComponent implements OnInit {

  username: string;
  password: string;

  isError: boolean = false;
  errMsg: string = '';

  constructor(private loginService: LoginService, private router: Router) { }

  ngOnInit() {
  }

  async doLogin() {
    try {
      var rs: any = await this.loginService.doLogin(this.username, this.password);

      console.log(rs);

      if (rs.ok) {
        // success
        sessionStorage.setItem('token', rs.token);
        this.router.navigateByUrl('/admin');
      } else {
        this.isError = true;
        this.errMsg = rs.error;
      }

    } catch (error) {
      console.log(error);
    }
  }
}
